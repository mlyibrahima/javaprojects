package com.interview.java.exercise6;

abstract class Animal {
    protected String name;

    public Animal(String name)
    {
        this.name = name;
    }

    public String getName()
    {
        return name;
    }
}

class Dog extends Animal {
    /**
     * Creates a new dog with the given name.
     */
    public Dog(String name) {
        super(name);
    }
}

class Cat extends Animal {
    /**
     * Creates a new cat with the given name.
     */
    public Cat(String name) {
        super(name);
    }
}

class Application {

    /**
     * @return the name of the given animal
     */
    static String getAnimalName(Animal a) {
        return a.getName();
    }
}
