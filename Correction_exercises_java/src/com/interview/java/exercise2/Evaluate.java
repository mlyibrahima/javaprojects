package com.interview.java.exercise2;

import org.junit.Test;
import org.junit.Assert;
import java.util.concurrent.ExecutorService;
import java.util.concurrent.Executors;
import java.util.stream.IntStream;
import java.util.concurrent.TimeUnit ;

public class Evaluate {
    @Test
    public void testExercise() {
        ExecutorService service = Executors.newFixedThreadPool(4);
        Counter counter = new Counter();
        try
        {
            IntStream.range(0, 1000)
                    .forEach(count -> service.submit(counter::increment));
            service.awaitTermination(1000, TimeUnit.MILLISECONDS);
        }
        catch(InterruptedException e)
        {
            System.out.println("error: "+e) ;
        }
        Assert.assertEquals("la méthode n'est pas synchronisée",1000, counter.getCount());
    }
}

