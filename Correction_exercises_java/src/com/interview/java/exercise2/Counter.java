package com.interview.java.exercise2;

class Counter{

    public  int count = 0;
    /**
     *increments count in a thread safe manner .
     **/
    public synchronized void increment() {
        setcount(getCount() + 1);
    }


    public void setcount(int count){
        this.count = count;
    }

    public int getCount(){
        return this.count;

    }
}