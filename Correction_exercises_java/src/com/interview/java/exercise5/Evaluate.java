package com.interview.java.exercise5;

import org.junit.Test;
import org.junit.Assert;


public class Evaluate {
    @Test
    public void testExercise() {
        B b = new B();
        A a = new A();
        Assert.assertEquals("b doit être une instance de A",true,  b instanceof A);
        Assert.assertEquals("a ne doit pas être une instance de B",false, a instanceof B);


    }
}
