package com.interview.java.exercise3;
import org.junit.Test;
import org.junit.Assert;

public class Evaluate {
    @Test
    public void testExercise() {
        int tab1[]= {80,5,5,39,9};
        Assert.assertEquals("Ne fonctionne pas quand le plus grand element est dans la position 0",
                80, Algorithm.findLargest(tab1));

        int tab2[]= {80};
        Assert.assertEquals("Ne fonctionne pas quand le tableau contien un seul element",
                80, Algorithm.findLargest(tab2));

        int tab3[]= {80,5,5,39,88};
        Assert.assertEquals("Ne fonctionne pas quand le plus grand element est à la fin du tableau",
                88, Algorithm.findLargest(tab3));

    }
}
