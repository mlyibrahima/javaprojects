package com.interview.java.exercise3;

public class Algorithm {

    static int findLargest(int[] numbers){
        int max = numbers[0];

        for(int val: numbers)
            max = Math.max(val,max);

        return max;
    }

}