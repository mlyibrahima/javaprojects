package com.interview.java.exercise8;

class Solution {
    /**
     * @return the sum of integers whose index is between n1 and n2
     */
    public static int calc(int[] array, int n1, int n2) {
        int som = 0;
        for(int i= n1; i<=n2;i++)
        {
            som+=array[i];
        }

        return som;
    }
}