package com.interview.java.exercise9;

public class StringUtils {

    static String concat(String[] strings) {

        StringBuilder val = new StringBuilder();
        for(String str: strings){
            val.append(str);
        }
        return val.toString();
    }
}