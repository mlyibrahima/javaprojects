package com.mycompany.dvdstore.web.api;

import com.mycompany.dvdstore.core.entity.Movie;
import com.mycompany.dvdstore.core.service.MovieServiceInterface;
import com.mycompany.dvdstore.web.form.MovieForm;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.validation.BindingResult;
import org.springframework.web.bind.annotation.*;

import javax.validation.Valid;
import java.util.List;

@RestController
@RequestMapping("/movie")
public class MovieResource {

    @Autowired
    private MovieServiceInterface movieService;

    public MovieServiceInterface getMovieService() {
        return movieService;
    }

    public void setMovieService(MovieServiceInterface movieService) {
        this.movieService = movieService;
    }

    @GetMapping("/{id}")
    public Movie get(@PathVariable("id") Long identifiant) {
        System.out.println("La méthode get a été invoquée");
        return movieService.getMovieById(identifiant);
    }

    @GetMapping
    public Iterable<Movie> list() {
        System.out.println("La méthode list a été invoquée");
        return movieService.getMovieList();
    }

    @PostMapping
    //Si j'enleve l'annotation @RequestBody sur les param de add ça marche
    public Movie add(Movie movie){
        System.out.println("La méthode add a été invoquée");
        return movieService.registerMovie(movie);
    }
}
